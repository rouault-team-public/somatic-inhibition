"""Compute the PMTHs and correlation function.

The datasets are stored in `.nwb` files.
"""

import logging
from importlib import resources
from pathlib import Path

import h5py  # type: ignore
import numpy as np
import scipy as sp  # type: ignore
import toml  # type: ignore
from matplotlib import pyplot as plt  # type: ignore
from numpy import typing as npt
from scipy import optimize, signal, stats

import config
from helpers import nwb_utils as nu

# Type annotations
Afloat = npt.NDArray[np.float64]
Abool = npt.NDArray[np.bool_]
Aint = npt.NDArray[np.int_]
Dpar = dict[str, dict]


def compute_autocorr(
    sign: Afloat,
    inter_conseq: Aint,
    mask: Abool,
    w_corr: int,
) -> Afloat:
    """Compute the autocorrelation with valid boundary condition.

    Args:
        sign (Afloat): Signal.
        inter_conseq (Aint): consecutive intervals on which to compute the corr
        mask (Abool): mask of the non-movement
        w_corr (int): index width of the returned correlation.

    Returns:
        Afloat: The correlogram.

    """
    sign_mask = sign[mask]
    sign_centr = sign - np.mean(sign_mask)

    mean_autocorr = np.zeros(w_corr)
    if inter_conseq.shape[0]:
        for inter in inter_conseq:
            if inter[1] - inter[0] < w_corr + 1:
                continue
            sign_inter = sign_centr[inter[0] : inter[1]]  # noqa: E203
            sign1 = sign_inter[w_corr:]
            autocorr = signal.correlate(
                sign1,
                sign_inter,
                mode='valid',
            )
            mean_autocorr += autocorr[:w_corr]
    else:
        logging.warning('No movement intervals!')

    return mean_autocorr


def compute_crosscorr(
    sign1: Afloat,
    sign2: Afloat,
    inter_conseq: Aint,
    mask: Abool,
    w_corr: int,
) -> Afloat:
    """Compute the cross-correlation of two signals.

    Args:
        sign1 (Afloat): Signal 1.
        sign2 (Afloat): Signal 2.
        inter_conseq (Aint): consecutive intervals on which to compute the corr
        mask (Abool): mask of the non-movement
        w_corr (int): index width of the returned correlation.

    Returns:
        Afloat: The correlogram.

    """
    sign1_mask = sign1[mask]
    sign1_centr = sign1 - np.mean(sign1_mask)
    sign2_mask = sign2[mask]
    sign2_centr = sign2 - np.mean(sign2_mask)

    mean_crosscorr = np.zeros(2 * w_corr)
    if inter_conseq.shape[0]:
        for inter in inter_conseq:
            if inter[1] - inter[0] < 2 * w_corr + 1:
                continue
            sign1_inter = sign1_centr[inter[0] : inter[1]]  # noqa: E203
            sign2_inter = sign2_centr[inter[0] : inter[1]]  # noqa: E203
            sign1_crop = sign1_inter[w_corr:-w_corr]
            crosscorr = signal.correlate(
                sign1_crop,
                sign2_inter,
                mode='valid',
            )
            mean_crosscorr += crosscorr[: 2 * w_corr]
    else:
        logging.warning('No movement intervals!')

    return mean_crosscorr


def compute_corrs(
    trial_fn: Path,
    h5_out=None,
    max_lag: float = 11.0,
):
    """Correlations of E or I activities.

    The correlations are saved in the h5_out hdf5 file.

    Args:
        trial_fn (Path): Path to the trial file
        h5_out: hdf5 file descriptor.
        max_lag (float): Max lag of the correlations

    """
    ras_t, ras_pyr, ras_inh = nu.load_raster_cell_type(trial_fn)

    # sampling rate
    fs = nu.get_frame_rate(trial_fn)
    w_corr = int(fs * max_lag) + 1

    # loading the non-movement intervals
    intervals = nu.load_all_periods(trial_fn)
    # loading the intervals after masking with a margin of 2 seconds
    mask, inter_conseq = nu.intervals_conseq(ras_t, intervals, margin=2.0)

    sign_e = np.mean(ras_pyr, axis=0)
    mean_autocorr_e = compute_autocorr(sign_e, inter_conseq, mask, w_corr)

    sign_i = np.mean(ras_inh, axis=0)
    mean_autocorr_i = compute_autocorr(sign_i, inter_conseq, mask, w_corr)

    mean_crosscorr = compute_crosscorr(
        sign_e,
        sign_i,
        inter_conseq,
        mask,
        w_corr,
    )

    n_val = mean_autocorr_e.size
    n_val_cross = mean_crosscorr.size
    t_axis_auto = np.linspace(0, (n_val - 1) / fs, n_val)
    t_axis_cross = np.linspace(
        -(n_val_cross // 2) / fs,
        (n_val_cross // 2) / fs,
        n_val_cross,
    )
    if h5_out is not None:
        h5_out.attrs['fs'] = fs
        h5_out.create_dataset('t_axis', data=t_axis_auto)
        h5_out.create_dataset('auto_e', data=mean_autocorr_e)
        h5_out.create_dataset('auto_i', data=mean_autocorr_i)
        h5_out.create_dataset('t_axis_cross', data=t_axis_cross)
        h5_out.create_dataset('cross_ei', data=mean_crosscorr)


def compute_movement_intervals(p_groups: tuple, nwbs: dict) -> list:
    """Compute the movement intervals.

    Args:
        p_groups (tuple): the p_day groups.
        nwbs (dict): nwb file dictionary.

    Returns:
        list: list of intervals per group

    """
    inter_grps = []
    for group in p_groups:
        grp_inter = []
        for p_day in group:
            for tr in nwbs[p_day]:
                ras_t, ras_pyr, ras_inh = nu.load_raster_cell_type(tr)

                # sampling rate
                fs = nu.get_frame_rate(tr)

                # loading the non-movement intervals
                intervals = nu.load_all_periods(tr)
                # loading the intervals after masking
                mvt_inter = nu.intervals_mvt(ras_t, intervals)
                grp_inter.append(mvt_inter / fs)
        grp_inter_np = np.concatenate(grp_inter)
        inter_grps.append(grp_inter_np[:, 1] - grp_inter_np[:, 0])

    return inter_grps


def lognorm_f(tspan: Afloat, sigma: float, mu: float) -> Afloat:
    """Lognormal function for curve_fit.

    Args:
        tspan (Afloat): time axis.
        sigma (float): sigma parameter.
        mu (float): mu parameter

    Returns:
        Afloat: the lognorm pdf.

    """
    return stats.lognorm.pdf(tspan, s=sigma, scale=np.exp(mu))


def movement_inter_hist(conf: dict[str, dict]):
    """Compute the histogram of movement intervals.

    Args:
        conf (dict): dictionary for the dataset configurations.

    """
    logging.info('List the .nwb files')
    nwbs = nu.gen_nwb_dict(Path('data') / conf['nwb_files']['dataset_folder'])

    logging.info('Computing the movement intervals.')
    p_groups = conf['ages']['day_groups']
    inter_grps = compute_movement_intervals(p_groups, nwbs)

    logging.info('Plotting the movement histogram.')
    plot_col = conf['plotting']['group_colors']
    n_grp = len(p_groups)
    fig, ax = plt.subplots(1, n_grp, figsize=(2.0 * n_grp, 2.0), sharey=True)
    max_lag = 15.0
    n_bins = 50
    n_fit = 200
    bins = np.linspace(0, max_lag, n_bins)
    bin_ctrs = (bins[:-1] + bins[1:]) / 2
    fit_tspan = np.linspace(0, max_lag, n_fit)
    for i_grp, inter in enumerate(inter_grps):
        d_fi = p_groups[i_grp][0][1:-1]
        d_la = p_groups[i_grp][-1][1:-1]
        bin_vals, _ = np.histogram(inter, bins=bins)
        norm = np.sum(bin_vals) * (bins[1] - bins[0])
        error = np.sqrt(bin_vals + 1) / norm
        vals_norm = bin_vals / norm
        popt, _ = optimize.curve_fit(
            lognorm_f,
            bin_ctrs,
            vals_norm,
            sigma=error,
            p0=[1.0, 0],
        )
        logging.info('Lognormal fit: sigma={0}, mu={1}'.format(*popt))
        ax[i_grp].hist(  # type: ignore
            inter,
            bins=bins,
            density=True,
            fill=False,
            edgecolor=plot_col[i_grp],
            lw=0.5,
            label='P{0}–{1}'.format(d_fi, d_la),
        )
        ax[i_grp].errorbar(  # type: ignore
            bin_ctrs,
            vals_norm,
            yerr=error,
            ls='none',
            color='black',
            elinewidth=0.5,
            capthick=0.5,
            capsize=0.5,
        )
        if i_grp == len(inter_grps) - 1:
            label = 'Log-normal fit'
        else:
            label = None
        ax[i_grp].plot(  # type: ignore
            fit_tspan,
            lognorm_f(fit_tspan, *popt),
            ':',
            color='black',
            lw='0.7',
            label=label,
        )
        ax[i_grp].set_xlabel('Mvt duration (s)')  # type: ignore
        ax[i_grp].set_xticks((0, 5.0, 10.0, 15.0))  # type: ignore
    ax[0].set_ylabel('Density')  # type: ignore
    ax[0].set_yticks((0, 0.5, 1.0))  # type: ignore
    h1, l1 = ax[0].get_legend_handles_labels()  # type: ignore
    h2, l2 = ax[1].get_legend_handles_labels()  # type: ignore
    ax[1].legend(h1 + h2, l1 + l2, frameon=False)  # type: ignore
    fig.savefig('./figures/histogram_move_inter.pdf')


def correlations_all_days(conf: dict[str, dict]):
    """Save computed correlations in data folder.

    Saved as a dictionary access with keys [p_day][tr] where tr is the filename
    of the experiment.

    Args:
        conf (dict): dictionary for the dataset configurations.

    """
    logging.info('List the .nwb files')
    nwbs = nu.gen_nwb_dict(Path('data') / conf['nwb_files']['dataset_folder'])

    logging.info('Computing the correlations for each day.')
    p_list = conf['ages']['p_list']
    with h5py.File(Path('./data/corr_all_days.hdf5'), 'w') as f_out:
        for p_day in p_list:
            logging.info('Analyzing p_day: {0}'.format(p_day))
            p_day_out = f_out.create_group(p_day)
            for tr in nwbs[p_day]:
                nwb_out = p_day_out.create_group(tr.stem)
                compute_corrs(tr, h5_out=nwb_out)


def interp_corrs(corr_type: str, conf: Dpar) -> tuple[Afloat, list]:
    """Interpolate the correlation to a common sampling rate.

    Args:
        corr_type (str): type of correlation (see group_corr_days).
        conf (Dpar): configuration dictionary.

    Returns:
        Afloat: time axis.
        list: the correlations for the different groups.

    """
    resample_fs = 12.0
    d_groups = conf['ages']['day_groups']

    if corr_type in {'ee', 'ii'}:
        time_inter = (0.0, 10.0)
        t_axis_lab = 't_axis'
        corr_lab = 'auto_{0}'.format(corr_type[0])
    elif corr_type == 'ei':
        time_inter = (-10.0, 10.0)
        t_axis_lab = 't_axis_cross'
        corr_lab = 'cross_ei'
    else:
        logging.error('Correlation type not recognized: {0}'.format(corr_type))

    n_pts = int((time_inter[1] - time_inter[0]) * resample_fs) + 1
    t_com = np.linspace(*time_inter, n_pts)

    mean_corrs_ret = []
    with h5py.File(Path('./data/corr_all_days.hdf5'), 'r') as f_in:
        for gr in d_groups:
            mean_corr = np.zeros(n_pts)
            for p_day in gr:
                d_corrs = f_in[p_day]
                for dset in d_corrs.keys():
                    t_axis = d_corrs[dset][t_axis_lab]
                    corr = d_corrs[dset][corr_lab]
                    corr_resam = sp.interpolate.interp1d(t_axis, corr)(t_com)
                    mean_corr += corr_resam
            mean_corr /= np.max(mean_corr)
            mean_corrs_ret.append(mean_corr)

    return t_com, mean_corrs_ret


def group_corr_days(corr_type: str, conf: Dpar):
    """Plot grouped avg correlations, groups are provided as tuples.

    Args:
        corr_type (str): 'ee' for pyramidal autocorrelation,
            'ii' for interneuron autocorrelation,
            'ei' for the cross-correlation.
        conf (Dpar): configuration dictionary.

    """
    if corr_type in {'ee', 'ii'}:
        fig, ax = plt.subplots(figsize=(2.0, 1.8))
        xticks = [0.0, 5.0, 10.0]
        xlim = (0.0, 10.0)
    elif corr_type == 'ei':
        fig, ax = plt.subplots(figsize=(2.45, 1.8))
        xticks = [-10.0, -5.0, 0.0, 5.0, 10.0]
        xlim = (-10.0, 10.0)
    else:
        logging.error('Correlation type not recognized: {0}'.format(corr_type))

    line_cols = conf['plotting']['group_colors']
    d_groups = conf['ages']['day_groups']

    t_com, mean_corrs = interp_corrs(corr_type, conf)
    for i_gr, gr in enumerate(d_groups):
        d_fi = gr[0][1:-1]
        d_la = gr[-1][1:-1]
        label = 'P{0}–{1}'.format(d_fi, d_la)
        line_col = line_cols[i_gr]
        ax.plot(  # type: ignore
            t_com,
            mean_corrs[i_gr],
            lw=1,
            label=label,
            color=line_col,
        )

    if corr_type == 'ee':
        ax.set_ylabel(r'$\mathregular{C_{EE}}$')
        ax.legend(frameon=False)  # type: ignore
    elif corr_type == 'ii':
        ax.set_ylabel(r'$\mathregular{C_{II}}$')
        ax.legend(frameon=False)  # type: ignore
    else:
        ax.set_ylabel(r'$\mathregular{C_{I\rightarrow E}}$')
        ax.legend(frameon=False, loc='upper left')  # type: ignore

    ax.set_xlabel('Lag (s)')
    ax.set_xticks(xticks)
    ax.set_xlim(*xlim)
    ax.set_ylim(-0.5, 1.0)
    ax.set_yticks((-0.5, 0.0, 0.5, 1.0))
    filename = './figures/groupd_corr_{0}.pdf'.format(corr_type)
    fig.savefig(filename)


def generate_all_corr_figs(conf: Dpar):
    """Plot and save all correlation functions for all days.

    Save the figures in the ./figures folder

    Args:
        conf (Dpar): configuration dictionary.

    """
    # Plot grouped averages
    group_corr_days('ee', conf=conf)
    group_corr_days('ii', conf=conf)
    group_corr_days('ei', conf=conf)


def main():
    """Start the analysis."""
    # Read the configuration file.
    with resources.path(config, 'datasets.toml') as datapath:
        data_config = toml.load(datapath)

    # histograms of the movement intervals
    movement_inter_hist(conf=data_config)

    # compute the correlation function
    correlations_all_days(conf=data_config)

    # plot all correlation functions and PMTHs
    generate_all_corr_figs(conf=data_config)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    # Loading the matplotlib style
    with resources.path(config, 'svgstyle.mplstyle') as mplpath:
        plt.style.use(mplpath)  # type: ignore
    # ignore the fonttools info
    logging.getLogger('fontTools').setLevel(logging.WARNING)

    main()
