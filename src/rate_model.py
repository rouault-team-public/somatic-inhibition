"""Compute the rate model."""

import logging
from importlib import resources

import numpy as np
import toml  # type: ignore
from matplotlib import pyplot as plt  # type: ignore
from numpy import typing as npt
from scipy import linalg, signal  # type: ignore

import config

# Type annotations
Afloat = npt.NDArray[np.float64]
Dpar = dict[str, dict]


def calcium_ker(tau: float, dt: float) -> Afloat:
    """Define the Calcium sensor kernel.

    Args:
        pars (Dpar): Parameter dictionary.

    Returns:
        Afloat: the kernel centered on 0.

    """
    t_width = 20.0
    ker_range = np.arange(-t_width, t_width, dt)
    gcamp_ker = np.exp(-ker_range / tau) / tau * dt
    gcamp_ker[ker_range < 0] = 0
    return gcamp_ker


def calcium_autocorr(tau: float, dt: float) -> Afloat:
    """Define the Calcium sensor kernel.

    Args:
        pars (Dpar): Parameter dictionary.

    Returns:
        Afloat: the kernel centered on 0.

    """
    t_width = 20.0
    ker_range = np.arange(-t_width, t_width, dt)
    gcamp_ker = np.exp(-np.abs(ker_range) / tau) / tau / 2 * dt
    return gcamp_ker


def get_w_sigma(
    pars: dict[str, np.float64],
    inh: str = 'weak',
) -> tuple[Afloat, Afloat, float]:
    """Output the connectivity matrix and noise.

    Args:
        pars (dict[str, np.float64]): Parameter dictionary
        inh (str): inhibition strength

    Returns:
        Afloat: decay rate
        Afloat: connectivity matrix
        float: noise variance

    """
    j_ee = pars['Jee']
    j_ii = pars['Jii']
    j_ie = pars['Jie']
    if inh == 'weak':
        j_ei = pars['Jei_weak']
    elif inh == 'strong':
        j_ei = pars['Jei_strong']
    else:
        logging.error(
            'Inhibition strength not supported: {0}'.format(inh),
        )
    tau_e = pars['tau_e']
    tau_i = pars['tau_i']
    noise_var = float(pars['noise_var'])
    decay = np.array([1 / tau_e, 1 / tau_i])
    w_mat = np.array(
        [
            [j_ee, j_ei],
            [j_ie, j_ii],
        ],
    )
    return decay, w_mat, noise_var


def fixed_point(pars: Dpar, inh: str = 'weak') -> Afloat:
    """Compute the steady state rates.

    Args:
        pars (Dpar): rate model parameters.
        inh (str): inhibition strength

    Returns:
        Afloat: Steady state rates.

    """
    decay, w_mat, _ = get_w_sigma(pars['dynamic_pars'], inh=inh)
    h_e0 = pars['external_drive']['he0']
    h_i0 = pars['external_drive']['hi0']
    a_mat = w_mat - np.diag(decay)
    logging.info('Eigen values: {0}'.format(linalg.eigvals(a_mat)))
    r0 = np.array([h_e0, h_i0])
    rates = -linalg.inv(a_mat) @ r0
    logging.info('Steady-state rates: {0}'.format(rates))
    return rates


def phi(cur: Afloat):
    """Non linearity of the neuron.

    Args:
        cur (Afloat): Input current.

    Returns:
        Afloat: Rectified output

    """
    cur_cp = np.copy(cur)
    return cur_cp * (cur_cp > 0)


def heaviside(cur: Afloat):
    """Diff of the non-linearity of the neuron.

    Args:
        cur (Afloat): Input current.

    Returns:
        Afloat: Rectified output

    """
    cur_cp = np.copy(cur)
    return (cur_cp > 0).astype(np.float64)


def integrate(
    pars: Dpar,
    tspan: Afloat,
    ext_in: Afloat,
    inh: str,
    rng,
) -> Afloat:
    """Euler integration.

    Args:
        pars (Dpar): rate model parameters.
        tspan (Afloat): time axis.
        ext_in (Afloat): External input.
        inh (str): inhibition strength.
        rng (Generator): random number generator.

    Returns:
        Afloat: output rates.

    """
    drive_par = pars['external_drive']
    decay, w_mat, sigma = get_w_sigma(pars['dynamic_pars'], inh=inh)
    h_in = np.array([drive_par['he0'], drive_par['hi0']])
    j_twi = np.array([drive_par['j_twi_e'], drive_par['j_twi_i']])
    if inh == 'strong':
        h_in *= drive_par['drive_strong']

    n_steps = tspan.size - 1
    dts = np.diff(tspan)
    sqrt_dts = np.sqrt(dts)
    noise_vals = rng.normal(size=(n_steps, 2))

    # initialize rates
    rates = np.zeros((n_steps + 1, 2))

    # integrate
    for n in range(n_steps):
        argu = w_mat @ rates[n] + h_in + j_twi * ext_in[n]
        f_ito = -decay * rates[n] + phi(argu)
        g_ito = heaviside(argu) * sigma @ noise_vals[n]
        rates[n + 1] = rates[n] + f_ito * dts[n] + g_ito * sqrt_dts[n]

    return rates


def weak_noise_corr(
    pars: Dpar,
    inh: str,
    max_lag=15.0,
) -> tuple[Afloat, Afloat]:
    """Weak noise limit expression of the correlation function.

    Args:
        pars (Dpar): rate model parameters.
        inh (str): inhibition strength.
        max_lag (float): maximum time to be computed.

    Returns:
        Afloat: time axis
        Afloat: correlation matrix[i, j, t] with i, j for the populations and
            t for the time index

    """
    decay, w_mat, noise_var = get_w_sigma(pars['dynamic_pars'], inh=inh)
    a_mat = -w_mat + np.diag(decay)
    p_mat = a_mat - np.trace(a_mat)
    det_a = linalg.det(a_mat)
    ident = np.identity(2)
    numerator = det_a * noise_var * ident + noise_var * p_mat @ p_mat.T
    denom = 2 * np.trace(a_mat) * det_a
    sigma = numerator / denom
    lags = np.arange(0, max_lag, pars['simulation']['dt'])
    c_mat = np.zeros((2, 2, lags.size))
    for i, lag in enumerate(lags):
        c_mat[:, :, i] = linalg.expm(-a_mat * lag) @ sigma

    # Convolving with the calcium kernel
    tau_calcium = pars['simulation']['tau_calcium']
    dt = pars['simulation']['dt']
    gcamp_ker = calcium_autocorr(tau_calcium, dt)
    c_mat[0, 0] = signal.convolve(c_mat[0, 0], gcamp_ker, mode='same')
    c_mat[1, 1] = signal.convolve(c_mat[1, 1], gcamp_ker, mode='same')
    c_mat[0, 1] = signal.convolve(c_mat[0, 1], gcamp_ker, mode='same')
    c_mat[1, 0] = signal.convolve(c_mat[1, 0], gcamp_ker, mode='same')

    # normalization
    c_mat[0, 0] /= np.max(c_mat[0, 0])
    c_mat[1, 1] /= np.max(c_mat[1, 1])
    c_mat[0, 1] /= np.max(c_mat[0, 1])
    c_mat[1, 0] /= np.max(c_mat[1, 0])

    return lags, c_mat


def compute_pmth(pars: Dpar, rng):
    """Generate the PMTHs.

    Args:
        pars (Dpar): rate model parameters.
        rng (type): random number generator.

    """
    fixed_point(pars)
    dt = pars['simulation']['dt']
    tau_calcium = pars['simulation']['tau_calcium']
    tspan = np.arange(-20.0, 20.0, dt)
    n_simu = pars['pmth']['n_simu']

    plot_pars = {'lw': 1}

    pmth_par = pars['pmth']
    for i_cell, cell in enumerate(('exc', 'inh')):
        fig, ax = plt.subplots(figsize=(2.0, 1.8))
        for i_inh, inh in enumerate(('weak', 'strong')):
            pmth_avg = np.zeros((tspan.size, 2))
            for _ in range(n_simu):
                lim_mov = rng.lognormal(
                    sigma=pmth_par['lognorm_sigma'],
                    mean=pmth_par['lognorm_mu'],
                )
                ext_in = np.zeros(tspan.size)
                mask = np.logical_and(tspan > 0, tspan < lim_mov)
                ext_in[mask] = 1.0
                rates = integrate(pars, tspan, ext_in=ext_in, inh=inh, rng=rng)
                pmth_avg += rates
            pmth_avg /= n_simu

            # Convolving with the calcium kernel
            gcamp_ker = calcium_ker(tau_calcium, dt)
            pmth_avg[:, 0] = signal.convolve(
                pmth_avg[:, 0],
                gcamp_ker,
                mode='same',
            )
            pmth_avg[:, 1] = signal.convolve(
                pmth_avg[:, 1],
                gcamp_ker,
                mode='same',
            )

            ax.plot(  # type: ignore
                tspan,
                pmth_avg[:, i_cell],
                label=pars['plotting']['labels'][i_inh],
                color=pars['plotting']['colors'][i_inh],
                **plot_pars,  # type: ignore
            )

        ylims = (
            (0, 6.1),
            (0, 18.0),
        )
        yticks = (
            (0, 3.0, 6.0),
            (0, 5.0, 10.0, 15.0),
        )
        ax.set_xlim(-5, 10)
        ax.set_ylim(*ylims[i_cell])
        ax.set_yticks(yticks[i_cell])
        ax.set_xticks([-5, 0, 5, 10])
        ax.set_xlabel('Lags (s)')
        ax.legend(frameon=False)  # type: ignore
        ax.axvline(0, color='k', ls='--', lw=0.5)  # type: ignore
        ax.set_ylabel('Rate (Hz)')

        figname = './figures/rate_pmth_{0}.pdf'.format(cell)
        fig.savefig(figname)


def model_corrs(pars: Dpar, inh: str, rng) -> tuple[tuple, tuple]:
    """Compute the correlations by integrating the model.

    Args:
        pars (Dpar): rate model parameters.
        inh (str): inhibition strength.
        rng (Generator): random number generator.

    Returns:
        tuple: time axis for the auto and cross correlations.
        tuple: auto and cross correlations

    """
    # simulation
    simu_par = pars['simulation']
    tspan = np.arange(0, simu_par['t_stop_corr'], simu_par['dt'])
    tspan -= simu_par['discard_t']
    ext_in = np.zeros(tspan.shape)
    rates = integrate(pars, tspan, ext_in=ext_in, inh=inh, rng=rng)

    # Convolving with the calcium kernel
    gcamp_ker = calcium_ker(simu_par['tau_calcium'], simu_par['dt'])
    rates[:, 0] = signal.convolve(rates[:, 0], gcamp_ker, mode='same')
    rates[:, 1] = signal.convolve(rates[:, 1], gcamp_ker, mode='same')

    # correlations
    win_corr = simu_par['corr_window']
    mask = tspan >= 0
    rate_e = rates[mask, 0]
    rate_e -= np.mean(rate_e)
    rate_i = rates[mask, 1]
    rate_i -= np.mean(rate_i)
    tspan = tspan[mask]

    rate_e_crop_l = rate_e[tspan > win_corr]
    rate_i_crop_l = rate_i[tspan > win_corr]
    mask_lr = np.logical_and(tspan > win_corr, tspan < tspan[-1] - win_corr)
    rate_e_crop_lr = rate_e[mask_lr]

    auto_e = signal.correlate(rate_e_crop_l, rate_e, mode='valid')
    auto_e /= np.max(auto_e)
    auto_i = signal.correlate(rate_i_crop_l, rate_i, mode='valid')
    auto_i /= np.max(auto_i)
    cross_ei = signal.correlate(rate_e_crop_lr, rate_i, mode='valid')
    cross_ei /= np.max(cross_ei)

    tspan_auto = tspan[: auto_e.size]
    tspan_cross = np.copy(tspan[: cross_ei.size])
    tspan_cross -= tspan_cross[-1] // 2

    return (tspan_auto, tspan_cross), (auto_e, auto_i, cross_ei)


def compute_correlations(conf: Dpar, rng):
    """Compute and plot the correlations.

    Args:
        conf (Dpar): configuration dictionary.
        rng: random number generator.

    """
    if conf['plotting']['companion']:
        fig_ee, ax_ee = plt.subplots(figsize=((1.8, 1.8)))
        fig_ii, ax_ii = plt.subplots(figsize=((1.8, 1.8)))
        fig_ei, ax_ei = plt.subplots(figsize=((2.25, 1.8)))
    else:
        fig_ee, ax_ee = plt.subplots(figsize=((2.0, 1.8)))
        fig_ii, ax_ii = plt.subplots(figsize=((2.0, 1.8)))
        fig_ei, ax_ei = plt.subplots(figsize=((2.45, 1.8)))

    for i_inh, inh in enumerate(['weak', 'strong']):
        lags, c_mat = weak_noise_corr(conf, inh=inh)
        tspan, auto_cross = model_corrs(conf, inh=inh, rng=rng)

        lab = conf['plotting']['labels'][i_inh]
        col = conf['plotting']['colors'][i_inh]

        pars_wn = {'linestyle': 'dotted', 'color': col, 'lw': 1}
        pars_mod = {'color': col, 'lw': 1, 'label': lab}
        ax_ee.plot(tspan[0], auto_cross[0], **pars_mod)
        ax_ee.plot(lags, c_mat[0, 0, :], **pars_wn)

        ax_ii.plot(tspan[0], auto_cross[1], **pars_mod)
        ax_ii.plot(lags, c_mat[1, 1, :], **pars_wn)

        ax_ei.plot(tspan[1], auto_cross[2], **pars_mod)
        ax_ei.plot(-lags, c_mat[1, 0, :], **pars_wn)
        ax_ei.plot(lags, c_mat[0, 1, :], **pars_wn)

    if conf['plotting']['companion']:
        # y labels are masked for the paper figures
        for ax in (ax_ee, ax_ii, ax_ei):
            [  # noqa: WPS
                t.set_color('white')
                for t in ax.yaxis.get_ticklabels()  # type: ignore
            ]
    else:
        ax_ee.set_ylabel(r'$\mathregular{C_{EE}}$')
        ax_ii.set_ylabel(r'$\mathregular{C_{II}}$')
        ax_ei.set_ylabel(r'$\mathregular{C_{EI}}$')

    for ax2 in (ax_ee, ax_ii, ax_ei):
        ax2.set_xlim(0, 10.0)
        ax2.set_xticks((0, 5.0, 10.0))
        ax2.set_ylim(-0.5, 1.0)
        ax2.set_yticks((-0.5, 0, 0.5, 1.0))
        ax2.legend(frameon=False)  # type: ignore
        ax2.set_xlabel('Lag (s)')
    ax_ei.set_xlim(-10.0, 10.0)
    ax_ei.set_xticks((-10.0, -5.0, 0, 5.0, 10.0))

    fig_ee.savefig('./figures/rate_cc_ee.pdf')
    fig_ii.savefig('./figures/rate_cc_ii.pdf')
    fig_ei.savefig('./figures/rate_cc_ei.pdf')


def main():
    """Start the script."""
    with resources.path(config, 'rate_model.toml') as datapath:
        model_config = toml.load(datapath)

    # initialize the random number generator
    rng = np.random.default_rng(12345)

    compute_correlations(model_config, rng=rng)

    compute_pmth(model_config, rng=rng)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    # Loading the matplotlib style
    with resources.path(config, 'svgstyle.mplstyle') as mplpath:
        plt.style.use(mplpath)  # type: ignore
    # ignore the fonttools info
    logging.getLogger('fontTools').setLevel(logging.WARNING)

    main()
