"""LIF simulation."""
import logging
from importlib import resources
from pathlib import Path

import h5py  # type: ignore
import numpy as np
import toml  # type: ignore
from matplotlib import pyplot as plt  # type: ignore
from numpy import typing as npt
from scipy import signal  # type: ignore

import config
import lifsimu  # type: ignore

# Type annotations
Afloat = npt.NDArray[np.float64]
Dpar = dict[str, dict]


def calcium_ker(pars: Dpar) -> Afloat:
    """Define the Calcium sensor kernel.

    Args:
        pars (Dpar): Parameter dictionary.

    Returns:
        Afloat: the kernel centered on 0.

    """
    tau_gc = pars['simu']['tau_calcium']
    w_bin = pars['simu']['w_bin']
    t_width = 20000.0
    ker_range = np.arange(-t_width, t_width, w_bin)
    gcamp_ker = np.exp(-ker_range / tau_gc) / tau_gc * w_bin
    gcamp_ker[ker_range < 0] = 0
    return gcamp_ker


def gen_ff_input_twitch(tspan: Afloat, pars: Dpar, rng) -> Afloat:
    """Generate the twitch feedforward input.

    Args:
        tspan (Afloat): time axis.
        pars (Dpar): lif model parameters.
        rng (Generator): numpy random number generator.

    Returns:
        Afloat: the feedforward input.

    """
    ff_in = np.zeros(tspan.size)
    lim_mov = rng.lognormal(
        sigma=pars['pmth']['lognorm_sigma'],
        mean=pars['pmth']['lognorm_mu'],
    )
    logging.info('stim duration: {0}'.format(lim_mov))
    mask = np.logical_and(tspan > 0, tspan < lim_mov * 1000)
    ff_in[mask] = 1

    return ff_in


def gen_ff_input_corr(tspan: Afloat, pars: Dpar) -> Afloat:
    """Generate the twitch feedforward input.

    Args:
        tspan (Afloat): time axis.
        pars (Dpar): lif model parameters.

    Returns:
        Afloat: the feedforward input.

    """
    return np.ones(tspan.size)


def run_twitch(inh: str, conf: dict[str, dict], rng) -> Afloat:
    """Simulate with the given inhibition strength.

    Args:
        inh (str): Inhibition strength.
        conf (Dpar): parameters for the lif model.
        rng (Generator): numpy random number generator.

    Returns:
        Afloat: binned output.

    """
    if inh == 'weak':
        conf['dynamics']['j_ei'] = conf['dynamics']['j_ei_weak']
        conf['dynamics']['he0'] = conf['dynamics']['he0_weak']
        conf['dynamics']['hi0'] = conf['dynamics']['hi0_weak']
        conf['dynamics']['je_twi'] = conf['dynamics']['je_twi_weak']
        conf['dynamics']['ji_twi'] = conf['dynamics']['ji_twi_weak']
    elif inh == 'strong':
        conf['dynamics']['j_ei'] = conf['dynamics']['j_ei_strong']
        conf['dynamics']['he0'] = conf['dynamics']['he0_strong']
        conf['dynamics']['hi0'] = conf['dynamics']['hi0_strong']
        conf['dynamics']['je_twi'] = conf['dynamics']['je_twi_strong']
        conf['dynamics']['ji_twi'] = conf['dynamics']['ji_twi_strong']
    else:
        logging.error(
            'Inhibition strength not recognized: {0}'.format(inh),
        )

    simu_par = conf['simu']
    tspan = np.arange(
        -simu_par['pre_time'],
        simu_par['t_stop_pmth'],
        simu_par['dt'],
    )
    ff_input = gen_ff_input_twitch(tspan, conf, rng)
    seed = rng.integers(np.iinfo(np.int64).max)
    simu_par['rng_seed'] = seed.item()
    return lifsimu.simu(tspan, ff_input, conf)


def pmth_avg(pars: Dpar) -> dict:
    """Take the average pmth from the hdf5 file.

    Returns:
        dict: dictionary containing the averages.

    """
    dict_out: dict[str, dict]
    dict_out = {}
    with h5py.File(Path('./data/lif_pmth.hdf5'), 'r') as f_in:
        for inh in ('weak', 'strong'):
            dict_out[inh] = {}
            g_in = f_in[inh]
            avg_resp = None
            for arr in g_in.values():
                if avg_resp is None:
                    avg_resp = np.zeros(arr.shape)
                avg_resp += arr[:]
            avg_resp /= len(g_in.keys())

            pop_e = avg_resp[:, 1]
            pop_i = avg_resp[:, 2]

            # convolving with the calcium kernel
            gcamp_ker = calcium_ker(pars)
            pop_e = signal.convolve(pop_e, gcamp_ker, mode='same')
            pop_i = signal.convolve(pop_i, gcamp_ker, mode='same')

            dict_out[inh]['tspan'] = avg_resp[:, 0]
            dict_out[inh]['exc'] = pop_e
            dict_out[inh]['inh'] = pop_i
    return dict_out


def twitch_pmth(pars: Dpar):
    """Compute twitch PMTH.

    Args:
        pars (Dpar): Description of parameter `pars`.

    """
    logging.info('Computing pmth')
    avg_resp = pmth_avg(pars)
    for pop in ('exc', 'inh'):
        fig, ax = plt.subplots(figsize=(2.0, 1.8))
        for i_inh, inh in enumerate(('weak', 'strong')):
            tspan = avg_resp[inh]['tspan'] * 1e-3
            resp_pop = avg_resp[inh][pop]

            plot_par = {'color': pars['plotting']['colors'][i_inh], 'lw': 1}
            ax.plot(  # type: ignore
                tspan,
                100 * resp_pop,
                **plot_par,
            )

        ax.set_xlim(-5, 10)
        # ax_pmth.set_ylim(0, 6.1)
        # ax.set_yticks([0, 3.0, 6.0])
        ax.set_xticks([-5, 0, 5, 10])
        v_line_par = {'color': 'black', 'ls': '--', 'lw': 1}
        ax.axvline(0, **v_line_par)  # type: ignore
        ax.set_xlabel('Lag (s)')
        ax.set_ylabel('Active fract. (%)')
        figname = './figures/pmth_lif_{0}.pdf'.format(pop)
        fig.savefig(figname)


def print_actives(bin_resp):
    """Print avg population active fraction."""
    tspan = bin_resp[:, 0]
    pop_e = bin_resp[tspan >= 0, 1]
    pop_i = bin_resp[tspan >= 0, 2]
    logging.info('f_E = {0:.4f}%'.format(np.mean(pop_e)))
    logging.info('f_I = {0:.4f}%'.format(np.mean(pop_i)))


def generate_pmth_responses(conf: Dpar, rng):
    """Generate PMTH responses.

    Args:
        conf (Dpar): model parameter dictionary.
        rng (Generator): numpy random number generator.

    """
    with h5py.File(Path('./data/lif_pmth.hdf5'), 'w') as f_out:
        for inh in ('weak', 'strong'):
            g_out = f_out.create_group(inh)
            n_simu = conf['pmth']['n_simu']
            for i_sim in range(n_simu):
                bin_resp = run_twitch(inh, conf, rng)
                g_out.create_dataset('simu{0}'.format(i_sim), data=bin_resp)


def model_corrs(pars: Dpar, inh: str, rng) -> tuple[tuple, tuple]:
    """Compute the correlations by integrating the model.

    Args:
        pars (Dpar): rate model parameters.
        inh (str): inhibition strength.
        rng (Generator): numpy random number generator.

    Returns:
        tuple: time axis for the auto and cross correlations.
        tuple: auto and cross correlations

    """
    # simulation
    if inh == 'weak':
        pars['dynamics']['j_ei'] = pars['dynamics']['j_ei_weak']
        pars['dynamics']['he0'] = pars['dynamics']['he0_weak']
        pars['dynamics']['hi0'] = pars['dynamics']['hi0_weak']
        pars['dynamics']['je_twi'] = pars['dynamics']['je_twi_weak']
        pars['dynamics']['ji_twi'] = pars['dynamics']['ji_twi_weak']
    elif inh == 'strong':
        pars['dynamics']['j_ei'] = pars['dynamics']['j_ei_strong']
        pars['dynamics']['he0'] = pars['dynamics']['he0_strong']
        pars['dynamics']['hi0'] = pars['dynamics']['hi0_strong']
        pars['dynamics']['je_twi'] = pars['dynamics']['je_twi_strong']
        pars['dynamics']['ji_twi'] = pars['dynamics']['ji_twi_strong']
    else:
        logging.error(
            'Inhibition strength not recognized: {0}'.format(inh),
        )
    simu_par = pars['simu']
    tspan = np.arange(
        -simu_par['pre_time'],
        simu_par['t_stop_corr'],
        simu_par['dt'],
    )
    seed = rng.integers(np.iinfo(np.int64).max)
    simu_par['rng_seed'] = seed.item()
    ff_input = np.zeros(tspan.size)
    bin_resp = lifsimu.simu(tspan, ff_input, pars)
    print_actives(bin_resp)

    # correlations
    mask = bin_resp[:, 0] >= 0
    pop_e = bin_resp[mask, 1]
    pop_e -= np.mean(pop_e)
    pop_i = bin_resp[mask, 2]
    pop_i -= np.mean(pop_i)

    # convolving with the calcium kernel
    gcamp_ker = calcium_ker(pars)
    pop_e = signal.convolve(pop_e, gcamp_ker, mode='same')
    pop_i = signal.convolve(pop_i, gcamp_ker, mode='same')
    tspan = bin_resp[mask, 0] * 1e-3

    win_corr = simu_par['corr_window']
    pop_e_crop_l = pop_e[tspan > win_corr]
    pop_i_crop_l = pop_i[tspan > win_corr]
    mask_lr = np.logical_and(tspan > win_corr, tspan < tspan[-1] - win_corr)
    pop_e_crop_lr = pop_e[mask_lr]

    auto_e = signal.correlate(pop_e_crop_l, pop_e, mode='valid')
    auto_e = auto_e[1:]
    auto_e /= np.max(auto_e)
    auto_i = signal.correlate(pop_i_crop_l, pop_i, mode='valid')
    auto_i = auto_i[1:]
    auto_i /= np.max(auto_i)
    cross_ei = signal.correlate(pop_i, pop_e_crop_lr, mode='valid')
    cross_ei /= np.max(cross_ei)

    tspan_auto = tspan[1 : auto_e.size + 1]  # noqa: E203
    tspan_cross = np.copy(tspan[: cross_ei.size])
    tspan_cross -= tspan_cross[-1] // 2

    return (tspan_auto, tspan_cross), (auto_e, auto_i, cross_ei)


def generate_corrs(pars: Dpar, rng):
    """Generate the correlations and save them in a hdf5 file.

    Args:
        pars (Dpar): model parameters.
        rng (Generator): numpy random number generator.

    """
    with h5py.File(Path('./data/lif_corrs.hdf5'), 'w') as f_out:
        for inh in ('weak', 'strong'):
            h5gr = f_out.create_group(inh)
            tspan, auto_cross = model_corrs(pars, inh, rng)
            h5gr.create_dataset('tspan_auto', data=tspan[0])
            h5gr.create_dataset('tspan_cross', data=tspan[1])
            h5gr.create_dataset('auto_e', data=auto_cross[0])
            h5gr.create_dataset('auto_i', data=auto_cross[1])
            h5gr.create_dataset('cross_ei', data=auto_cross[2])


def compute_corrs(pars: Dpar):
    """Compute the correlations.

    Args:
        pars (Dpar): LIF parameters dictionary.

    """
    fig_ee, ax_ee = plt.subplots(figsize=((1.8, 1.8)))
    fig_ii, ax_ii = plt.subplots(figsize=((1.8, 1.8)))
    fig_ei, ax_ei = plt.subplots(figsize=((2.25, 1.8)))

    # for i_inh, inh in enumerate(['weak']):
    with h5py.File(Path('./data/lif_corrs.hdf5'), 'r') as f_in:
        for i_inh, inh in enumerate(['weak', 'strong']):
            g_in = f_in[inh]

            lab = pars['plotting']['labels'][i_inh]
            col = pars['plotting']['colors'][i_inh]

            pars_mod = {'color': col, 'lw': 1, 'label': lab}
            ax_ee.plot(g_in['tspan_auto'][:], g_in['auto_e'][:], **pars_mod)
            ax_ii.plot(g_in['tspan_auto'][:], g_in['auto_i'][:], **pars_mod)
            ax_ei.plot(
                g_in['tspan_cross'][:][::-1],
                g_in['cross_ei'][:],
                **pars_mod,
            )

    # ax_ee.set_ylabel(r'$\mathregular{C_{EE}}$')
    # ax_ii.set_ylabel(r'$\mathregular{C_{II}}$')
    # ax_ei.set_ylabel(r'$\mathregular{C_{EI}}$')
    for ax in (ax_ee, ax_ii, ax_ei):
        ax.set_xlim(0, 10.0)
        ax.set_xticks((0, 5.0, 10.0))
        ax.set_ylim(-0.5, 1.0)
        ax.set_yticks((-0.5, 0, 0.5, 1.0))
        ax.legend(frameon=False)  # type: ignore
        ax.set_xlabel('Lag (s)')
        [  # noqa: WPS
            t.set_color('white')
            for t in ax.yaxis.get_ticklabels()  # type: ignore
        ]
    ax_ei.set_xlim(-10.0, 10.0)
    ax_ei.set_xticks((-10.0, -5.0, 0, 5.0, 10.0))

    fig_ee.savefig('./figures/lif_cc_ee.pdf')
    fig_ii.savefig('./figures/lif_cc_ii.pdf')
    fig_ei.savefig('./figures/lif_cc_ei.pdf')


def main():
    """Start the script."""
    with resources.path(config, 'lif_model.toml') as datapath:
        model_config = toml.load(datapath)

    # initialize the random number generator
    rng = np.random.default_rng(12345)

    # compute and save PMTH figures
    generate_pmth_responses(model_config, rng)
    twitch_pmth(model_config)

    # compute correlations and save figures
    generate_corrs(model_config, rng)
    compute_corrs(model_config)


if __name__ == '__main__':
    # execute only if run as a script
    logging.basicConfig(level=logging.INFO)

    # Loading the matplotlib style
    with resources.path(config, 'svgstyle.mplstyle') as mplpath:
        plt.style.use(mplpath)  # type: ignore
    # ignore the fonttools info
    logging.getLogger('fontTools').setLevel(logging.WARNING)

    main()
