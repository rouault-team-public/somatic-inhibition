#include <math.h>
#include <stdio.h>

#include "Python.h"
#include "globals.hpp"
#include "numpy/arrayobject.h"
#include "utils.hpp"

/* Global variables */
double *tspan;
double *ff_input;

/* Output arrays */
PyArrayObject *py_binned_out, *py_vm_out;

static PyObject *sim_lif(PyObject *self, PyObject *args);

static PyMethodDef simlif_methods[] = {
    {"simu", sim_lif, METH_VARARGS, "LIF network"},
    {NULL, NULL, 0, NULL} /* Sentinel */
};

static struct PyModuleDef simlifmodule = {PyModuleDef_HEAD_INIT, "lif",
                                          "", /* module documentation*/
                                          -1, simlif_methods};

PyMODINIT_FUNC PyInit_lifsimu(void) {
  import_array();
  return PyModule_Create(&simlifmodule);
}

static PyObject *sim_lif(PyObject *dummy, PyObject *args) {
  /* Processing the arguments */
  PyObject *py_tspan = NULL;
  PyObject *py_ff_input = NULL;
  PyObject *pars = NULL;

  if (!PyArg_ParseTuple(args, "O!O!O!", &PyArray_Type, &py_tspan, &PyArray_Type,
                        &py_ff_input, &PyDict_Type, &pars))
    return NULL;

  n_steps = PyArray_DIMS((PyArrayObject *)py_tspan)[0];
  tspan = (double *)PyArray_DATA((PyArrayObject *)py_tspan);
  ff_input = (double *)PyArray_DATA((PyArrayObject *)py_ff_input);

  PyObject *connect =
      PyDict_GetItemWithError(pars, PyUnicode_FromString("connectivity"));
  PyObject *dynamics =
      PyDict_GetItemWithError(pars, PyUnicode_FromString("dynamics"));
  PyObject *ext_drive =
      PyDict_GetItemWithError(pars, PyUnicode_FromString("ext_drive"));
  PyObject *simu = PyDict_GetItemWithError(pars, PyUnicode_FromString("simu"));

  PyObject *py_n_exc =
      PyDict_GetItemWithError(connect, PyUnicode_FromString("n_exc"));
  NE = PyLong_AsLong(py_n_exc);

  PyObject *py_n_inh =
      PyDict_GetItemWithError(connect, PyUnicode_FromString("n_inh"));
  NI = PyLong_AsLong(py_n_inh);

  N = NE + NI;

  PyObject *py_k_exc =
      PyDict_GetItemWithError(connect, PyUnicode_FromString("k_exc"));
  K = PyLong_AsLong(py_k_exc);

  PyObject *py_k_inh =
      PyDict_GetItemWithError(connect, PyUnicode_FromString("k_inh"));
  K_I = PyLong_AsLong(py_k_inh);

  PyObject *py_tau_mem =
      PyDict_GetItemWithError(dynamics, PyUnicode_FromString("tau_mem"));
  tau_membrane = PyFloat_AsDouble(py_tau_mem);

  PyObject *py_syn_e =
      PyDict_GetItemWithError(dynamics, PyUnicode_FromString("tau_syn_e"));
  tau_syn_e = PyFloat_AsDouble(py_syn_e);
  PyObject *py_syn_i =
      PyDict_GetItemWithError(dynamics, PyUnicode_FromString("tau_syn_i"));
  tau_syn_i = PyFloat_AsDouble(py_syn_i);

  PyObject *py_v_thr =
      PyDict_GetItemWithError(dynamics, PyUnicode_FromString("v_threshold"));
  V_threshold = PyFloat_AsDouble(py_v_thr);
  PyObject *py_v_reset =
      PyDict_GetItemWithError(dynamics, PyUnicode_FromString("v_reset"));
  V_reset = PyFloat_AsDouble(py_v_reset);

  PyObject *py_h_e0 =
      PyDict_GetItemWithError(dynamics, PyUnicode_FromString("he0"));
  He0 = PyFloat_AsDouble(py_h_e0);
  PyObject *py_h_i0 =
      PyDict_GetItemWithError(dynamics, PyUnicode_FromString("hi0"));
  Hi0 = PyFloat_AsDouble(py_h_i0);
  PyObject *py_j_e_twi =
      PyDict_GetItemWithError(dynamics, PyUnicode_FromString("je_twi"));
  Je_twi = PyFloat_AsDouble(py_j_e_twi);
  PyObject *py_j_i_twi =
      PyDict_GetItemWithError(dynamics, PyUnicode_FromString("ji_twi"));
  Ji_twi = PyFloat_AsDouble(py_j_i_twi);

  PyObject *py_j_ee =
      PyDict_GetItemWithError(dynamics, PyUnicode_FromString("j_ee"));
  Jee = PyFloat_AsDouble(py_j_ee);
  PyObject *py_j_ii =
      PyDict_GetItemWithError(dynamics, PyUnicode_FromString("j_ii"));
  Jii = PyFloat_AsDouble(py_j_ii);
  PyObject *py_j_ie =
      PyDict_GetItemWithError(dynamics, PyUnicode_FromString("j_ie"));
  Jie = PyFloat_AsDouble(py_j_ie);
  PyObject *py_j_ei =
      PyDict_GetItemWithError(dynamics, PyUnicode_FromString("j_ei"));
  Jei = PyFloat_AsDouble(py_j_ei);

  PyObject *py_noise_pwr =
      PyDict_GetItemWithError(ext_drive, PyUnicode_FromString("noise_power"));
  noise_power = PyFloat_AsDouble(py_noise_pwr);

  PyObject *py_dt = PyDict_GetItemWithError(simu, PyUnicode_FromString("dt"));
  dt = PyFloat_AsDouble(py_dt);
  PyObject *py_w_bin =
      PyDict_GetItemWithError(simu, PyUnicode_FromString("w_bin"));
  w_bin = PyFloat_AsDouble(py_w_bin);

  display_params();

  /* creating the output arrays */
  PyArray_Descr *descr64 = PyArray_DescrFromType(NPY_FLOAT64);
  npy_intp dims[2] = {
      (npy_intp)((tspan[n_steps - 1] - tspan[0]) / w_bin + 1),
      (npy_intp)5,
  };
  std::cout << "bin array size: " << dims[0] << std::endl;
  py_binned_out = (PyArrayObject *)PyArray_NewFromDescr(
      &PyArray_Type, descr64, 2, dims, NULL, NULL, 0, NULL);

  PyObject *py_rngseed =
      PyDict_GetItemWithError(simu, PyUnicode_FromString("rng_seed"));
  unsigned long rng_seed = PyLong_AsUnsignedLong(py_rngseed);
  init_rng(rng_seed);

  init_state_vectors();

  gen_conmat();

  integrate();

  std::cout << "deleting state vectors" << std::endl;
  delete_state_vectors();
  delete_rng();

  std::cout << "returning values" << std::endl;

  return Py_BuildValue("O", py_binned_out);
}
