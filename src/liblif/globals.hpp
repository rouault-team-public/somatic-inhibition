#ifndef _GLOBAL_VARS
#define _GLOBAL_VARS

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION

//
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
//
#include <assert.h>

#include <algorithm>
#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream> // required for libconfig++
#include <libconfig.h++>
#include <random>
#include <vector>

#include "Python.h"
#include "numpy/arrayobject.h"

// network size and connectivity
extern unsigned int NE, NI, N, K, K_I;
extern double sqrt_K, prob_recip;

extern unsigned long long int n_connections;

extern double dt;

// lif state vectors
extern double *Vm;

// lif params
extern double V_rest, V_threshold, V_reset;
// double d_threshold; // if a neuron spikes, increase its threshold by this
// value

extern double tau_membrane;

extern double He0, Hi0, Je_twi, Ji_twi, Jee, Jei, Jie, Jii;

// synaptic time constants
extern double tau_syn_e, tau_syn_i;

// external input
extern double noise_power;

/* input arrays */
extern size_t n_steps;
extern double *tspan;
extern double *ff_input;
extern double w_bin;

/* output arrays */
extern PyArrayObject *py_binned_out;

#endif
