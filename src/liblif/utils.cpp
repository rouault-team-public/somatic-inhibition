#ifndef _UTILS
#define _UTILS
#include <vector>

#include "globals.hpp"
#include "numpy/arrayobject.h"
//

using namespace libconfig;

// random number generators
std::mt19937 *rng_mt;

// network size and connectivity
unsigned int NE, NI, N, K, K_I;
double sqrt_K, prob_recip;

unsigned long long int n_connections;

double cur_t;
double dt;
size_t n_steps;
double w_bin;

// lif state vectors
double *Vm;

// lif params
double V_rest, V_threshold, V_reset;
// double d_threshold; // if a neuron spikes, increase its threshold by this
// value

double tau_membrane; // , tau_threshold;
double *g_e, *g_i, *g_ff;

// synaptic strengths
double He0, Hi0, Je_twi, Ji_twi, Jee, Jei, Jie, Jii;

// synaptic time constants
double tau_syn_e, tau_syn_i;

// transmission delay
double delay_syn;
int n_delay_bins = 1;
int **syn_delay_buffer = NULL;

// external input
double t, noise_power;

//
unsigned int *nPostNeurons = NULL, *sparseConVec = NULL, *idxVec = NULL;
unsigned int *n_pre_neurons = NULL, *pre_sparseVec = NULL, *pre_idxVec = NULL;

//
unsigned int n_spikes[] = {0, 0};
double pop_active_e = 0, pop_active_i = 0;
double pop_input_e = 0, pop_input_i = 0;

// initialize the RNG
void init_rng(std::uint_fast32_t seed) { rng_mt = new std::mt19937(seed); }

// ----------------------------------------- //
unsigned int *int_vector(unsigned int n, unsigned int init_val) {
  unsigned int *x = (unsigned int *)malloc(n * sizeof(unsigned int));
  // init to zero
  for (unsigned int i = 0; i < n; ++i) {
    x[i] = init_val;
  }
  return x;
}

double *double_vector(unsigned int n, double init_val) {
  double *x = (double *)malloc(n * sizeof(double));
  for (unsigned int i = 0; i < n; ++i) {
    x[i] = init_val;
  }
  return x;
}

double *double_vector(unsigned long long n, double init_val) {
  double *x = (double *)malloc(n * sizeof(double));
  for (unsigned int i = 0; i < n; ++i) {
    x[i] = init_val;
  }
  return x;
}

int **create_2d_matrix(int rows, int clmns, int init_val) {
  int **arr = new int *[rows];
  // arr = new int* [rows];
  for (int i = 0; i < rows; ++i) {
    arr[i] = new int[clmns];
  }
  // initialize
  for (int i = 0; i < rows; ++i) {
    for (int j = 0; j < clmns; ++j) {
      arr[i][j] = init_val;
    }
  }
  std::cout << "allocation done" << std::endl;
  return arr;
}

void clear_2d_matrix(int **arr, int rows) {
  for (int i = 0; i < rows; ++i) {
    delete[] arr[i];
  }
  delete[] arr;
}

void GenSparseMat(unsigned int *conVec, unsigned int rows, unsigned int clms,
                  unsigned int *sparseVec, unsigned int *idxVec,
                  unsigned int *nPostNeurons) {
  /* generate sparse representation
     conVec       : input vector / flattened matrix
     sparseVec    : sparse vector
     idxVec       : every element is the starting index in sparseVec
                    for ith row in matrix conVec
     nPostNeurons : number of non-zero elements in ith row
  */
  // printf("\n MAX Idx of conmat allowed = %u \n", rows * clms);
  size_t counter = 0;
  for (size_t i = 0; i < rows; ++i) {
    size_t nPost = 0;
    for (size_t j = 0; j < clms; ++j) {
      // printf("%llu %llu %llu %llu\n", i, j, i + clms * j, i + rows * j);
      if (conVec[i + rows * j]) { /* i --> j  */
        sparseVec[counter] = j;
        counter += 1;
        nPost += 1;
      }
    }
    nPostNeurons[i] = nPost;
  }

  idxVec[0] = 0;
  for (size_t i = 1; i < rows; ++i) {
    idxVec[i] = idxVec[i - 1] + nPostNeurons[i - 1];
  }
}

void gen_conmat() {
  // generates a connectivity matrix with fixed in-degree
  // size_t n_post_e=0, n_post_i = 0;
  n_connections = 0;
  unsigned int *conmat = int_vector(N * N, 0);
  std::vector<int> pre_E_neurons(NE);
  std::vector<int> pre_I_neurons(NI);
  // post synaptic neuron j recieves K and K_I inputs
  for (size_t j = 0; j < N; ++j) {
    std::iota(pre_E_neurons.begin(), pre_E_neurons.end(), 0);
    std::iota(pre_I_neurons.begin(), pre_I_neurons.end(), NE);
    std::shuffle(pre_E_neurons.begin(), pre_E_neurons.end(), *rng_mt);
    std::shuffle(pre_I_neurons.begin(), pre_I_neurons.end(), *rng_mt);
    unsigned int cntr = 0;
    // every neuron receives K inputs from excitatory neurons
    for (size_t i = 0; i < K; i++) {
      unsigned int pre = pre_E_neurons[i];
      if (pre == j)
        continue;
      conmat[pre + N * j] = 1;
      ++cntr;
      ++n_connections;
    }
    // every neuron receives K_I inputs from inhibitory neurons
    for (size_t i = 0; i < K_I; i++) {
      unsigned int pre = pre_I_neurons[i];
      if (pre == j)
        continue;
      conmat[pre + N * j] = 1;
      ++cntr;
      ++n_connections;
    }
  }

  unsigned int new_ncon = 0;
  for (size_t i = 0; i < N; ++i) {
    for (size_t j = 0; j < N; ++j) {
      if (conmat[i + N * j]) {
        ++new_ncon;
      }
    }
  }
  std::cout << " connections done!" << std::endl;
  std::cout << "new nc = " << new_ncon << std::endl;
  std::cout << n_connections << std::endl;

  size_t n_cons_fixed = (K + K_I) * N;
  std::cout << n_cons_fixed << std::endl;
  assert(n_connections == n_cons_fixed);

  sparseConVec = int_vector(n_cons_fixed, 0);
  GenSparseMat(conmat, N, N, sparseConVec, idxVec, nPostNeurons);

  free(conmat);
}

int display_params() {
  std::cout << "-- -- -- --- Network --- -- -- --" << std::endl;
  std::cout << "NE = " << NE << std::endl;
  std::cout << "NI = " << NI << std::endl;
  std::cout << "CE = " << K << std::endl;
  std::cout << "CI = " << K_I << std::endl;
  std::cout << "V_reset = " << V_reset << "mV" << std::endl;
  std::cout << "noise amplitude = " << noise_power << std::endl;
  std::cout << "tau membrane = " << tau_membrane << std::endl;
  std::cout << "firing threshold theta = " << V_threshold << "mV" << std::endl;
  std::cout << "weight matrix" << std::endl;
  std::cout << Jee << " " << Jei << std::endl;
  std::cout << Jie << " " << Jii << std::endl;
  std::cout << "-- -- --- Synaptic time constants --- -- --" << std::endl;
  std::cout << "tau_e = " << tau_syn_e << "ms  \ntau_i = " << tau_syn_i << "ms"
            << std::endl;
  std::cout << "-- -- --- ~~~~~~ ~~~~~~ --- -- --" << std::endl;

  return 0;
}

void init_state_vectors() {
  std::cout << " -- -- --- setup --- -- -- " << std::endl;
  // this function must be called after calling read_params
  Vm = double_vector(N, V_reset);
  g_e = double_vector(N, 0);
  g_ff = double_vector(N, 0);
  g_i = double_vector(N, 0);
  nPostNeurons = int_vector(N, K + K_I);
  idxVec = int_vector(N, 0);
  // buffer to store previous spikes upto (t - D)
  if (delay_syn > 0) {
    n_delay_bins = (int)(delay_syn / dt);
    std::cout << "# delay bins " << n_delay_bins << std::endl;
    syn_delay_buffer = create_2d_matrix(N, n_delay_bins, 0);
  }
}

void delete_state_vectors() {
  // call from main
  free(Vm);
  free(g_i);
  free(g_e);
  free(g_ff);
  free(nPostNeurons);
  free(idxVec);
  free(sparseConVec);
  if (delay_syn > 0) {
    clear_2d_matrix(syn_delay_buffer, N);
  }
}

void delete_rng() {
  // call from main
  delete rng_mt;
}

void get_pop_actives() {
  pop_active_e = (double)n_spikes[0] / NE;
  pop_active_i = (double)n_spikes[1] / NI;
  /* } */
  n_spikes[0] = 0;
  n_spikes[1] = 0;
}

void get_pop_inputs(double time_interval) {}

void propagate_spikes(unsigned int pre_neuron_idx) {
  // update all the post synaptic neurons to presynaptic neuron
  unsigned int tmpIdx, post_neuron_idx;
  // unsigned int n_spikes_e = 0, n_spikes_i = 0;
  // n_spikes[0] = 0;
  // n_spikes[1] = 0;
  tmpIdx = idxVec[pre_neuron_idx];
  if (pre_neuron_idx < NE) {
    n_spikes[0] += 1;
  } else {
    n_spikes[1] += 1;
  }
  for (size_t i = 0; i < nPostNeurons[pre_neuron_idx]; i++) {
    post_neuron_idx = sparseConVec[tmpIdx + i];
    if (pre_neuron_idx < NE) {
      /* --    E-to-E    -- */
      if (post_neuron_idx < NE)
        g_e[post_neuron_idx] += Jee;
      /* --    E-to-I    -- */
      else
        g_e[post_neuron_idx] += Jie;
    } else {
      /* --    I-to-E    -- */
      if (post_neuron_idx < NE)
        g_i[post_neuron_idx] += Jei;
      /* --    I-to-I    -- */
      else
        g_i[post_neuron_idx] += Jii;
    }
  }
}

void detect_spikes(double t) {
  // detect if Vm > V_threshold and add 1 to g_x vector
  for (unsigned int neuron_idx = 0; neuron_idx < N; ++neuron_idx) {
    if (Vm[neuron_idx] >= V_threshold) {
      Vm[neuron_idx] = V_reset;
      propagate_spikes(neuron_idx);
    }
  }
}

void ProgressBar(float progress, float me, float mi) {
  int barWidth = 31;
  std::cout << "Progress: [";
  int pos = barWidth * progress;
  for (int i = 0; i < barWidth; ++i) {
    if (i < pos)
      std::cout << "\u25a0";
    else
      std::cout << " ";
  }
  std::cout << std::fixed;
  std::cout << "] " << int(progress * 100.0)
            << "% done | << t = " << std::setprecision(2) << cur_t * 1e-3
            << " mE = " << std::setprecision(2) << me
            << " mI = " << std::setprecision(2) << mi << "\r";
  std::cout.flush();
  if (progress == 1.) {
    std::cout << std::fixed;
    std::cout << "] " << int(progress * 100.0)
              << "% done | << t = " << std::setprecision(2) << cur_t * 1e-3
              << " mE = " << std::setprecision(2) << me
              << " mI = " << std::setprecision(2) << mi << std::endl;
    std::cout.flush();
  }
}

void integrate() {
  double dt_over_tau;
  dt_over_tau = dt / tau_membrane;

  std::cout << " -- -- --- ~~~~~ --- -- -- " << std::endl;
  std::cout << "Euler Integration" << std::endl;
  std::cout << "dt = " << dt << std::endl;
  std::cout << "n steps = " << n_steps << std::endl;

  unsigned int n_bins = round(w_bin / dt); // x ms bins
  double time_interval = 1e-3 * dt * n_bins;
  std::cout << "n_bins = " << n_bins << " tim win = " << time_interval << "s"
            << std::endl;
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - */
  double input_ext = 0.0, net_input = 0.0;
  double exp_decay_e = exp(-dt / tau_syn_e);
  double exp_decay_i = exp(-dt / tau_syn_i);
  //
  // all neurons receive external inputs from K independent Poisson generators
  // with a rate of v_ext, which is the same as a input from a Poisson
  // gen of rate ext_rate = (K * v_ext)

  std::normal_distribution<double> normal_distr(0.0, 1.0);
  double sqrt_dt = sqrt(dt);

  size_t i_bin = 0;
  for (size_t t_i = 0; t_i < n_steps; ++t_i) {
    cur_t = tspan[t_i];
    for (size_t neuron_idx = 0; neuron_idx < N; ++neuron_idx) {
      // variable g_e and g_i store the total input from recurent spikes
      // exponential synapses
      g_e[neuron_idx] *= exp_decay_e;
      g_i[neuron_idx] *= exp_decay_i;
      // adaptive threshold
      // V_threshold[neuron_idx] += dt_over_tau_th *
      // (V_theshold_initial - V_threshold[neuron_idx]);
      // update membrane voltage only if the neuron is not in the ref. period
      if (neuron_idx < NE)
        input_ext = He0 + Je_twi * ff_input[t_i];
      else
        input_ext = Hi0 + Ji_twi * ff_input[t_i];

      //  dV/dt = -V / tau_m + Jii * S + I_ext
      net_input = input_ext + g_e[neuron_idx] + g_i[neuron_idx];
      Vm[neuron_idx] = (1.0 - dt_over_tau) * Vm[neuron_idx] + dt * net_input +
                       noise_power * sqrt_dt * normal_distr(*rng_mt);

      if (neuron_idx >= NE) {
        pop_input_e += dt * g_e[neuron_idx]; // h_bi
        pop_input_i += dt * g_i[neuron_idx]; // h_bi
      }
    }

    /* - - - */
    // detect spikes
    detect_spikes(cur_t); // detects spkikes and calls propagate_spikes()
    /* - - - - - */
    if (t_i % n_bins == 0) {
      // update
      get_pop_actives();
      ProgressBar((float)t_i / (float)n_steps, pop_active_e, pop_active_i);
      // save pop avg
      double *bin_out = (double *)PyArray_GETPTR2(py_binned_out, i_bin, 0);
      bin_out[0] = cur_t;
      bin_out[1] = pop_active_e;
      bin_out[2] = pop_active_i;
      bin_out[3] = pop_input_e;
      bin_out[4] = pop_input_i;

      pop_input_e = 0;
      pop_input_i = 0;
      i_bin++;
    }

  } // end of time loop
  std::cout << std::endl;
  std::cout << "last bin index: " << i_bin << std::endl;
}

#endif
