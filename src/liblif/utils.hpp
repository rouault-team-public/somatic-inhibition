#ifndef _UTILS
#define _UTILS

#include <stdio.h>

void integrate();
void ProgressBar(float progress, float me, float mi);
void get_pop_actives();
void delete_state_vectors();

void init_rng(std::uint_fast32_t seed);
void delete_rng();

void init_state_vectors();
int display_params();
void gen_conmat();
void GenSparseMat(unsigned int *conVec, unsigned int rows, unsigned int clms,
                  unsigned int *sparseVec, unsigned int *idxVec,
                  unsigned int *nPostNeurons);
void clear_2d_matrix(int **arr, int rows);
int **create_2d_matrix(int rows, int clmns, int init_val);
double *double_vector(unsigned int n, double init_val);
double *double_vector(unsigned long long n, double init_val);
unsigned int *int_vector(unsigned int n, unsigned int init_val);

#endif
