"""nwb file management."""

import logging
from pathlib import Path
from typing import Any

import numpy as np
from numpy import typing as npt
from pynwb import NWBHDF5IO  # type: ignore

# Type annotations
Afloat = npt.NDArray[np.float64]
Abool = npt.NDArray[np.bool_]
Aint = npt.NDArray[np.int_]


def load_raster(filename: Path) -> tuple[Afloat, Aint]:
    """Load a raster.

    Args:
        filename (Path): .nwb filename.

    Returns:
        Afloat: timestamps
        Aint: raster

    """
    nwbfile = NWBHDF5IO(filename, 'r')
    nwbfile = nwbfile.read()
    raster = (
        nwbfile.processing['ophys']
        .data_interfaces['fluorescence_all_cells']
        .roi_response_series['raster dur v26_5_v37_6']
        .data[:]
    )
    timestamps = (
        nwbfile.processing['ophys']
        .data_interfaces['fluorescence_all_cells']
        .roi_response_series['raster dur v26_5_v37_6']
        .timestamps[:]
    )
    # Some datasets have inconsistent timestamps and raster sizes:
    if timestamps.shape[0] > raster.shape[0]:
        logging.warning('Inconsistent size for timestamps and raster!')
        logging.warning('Shortening the timestamp at the end.')
        logging.warning('File {0}'.format(filename.name))
        timestamps = timestamps[: raster.shape[0]]
    return timestamps, raster.T


def load_cell_types(
    filename: Path,
) -> tuple[Any, Abool]:
    """Load cell types.

    Args:
        filename (Path): .nwb file.

    Returns:
        Any: cell type
        Abool: boolean array 'is pyramidal'.

    """
    # returns a list of ['interneuron', 'pyramidal', 'red_ins']
    # red_ins: labeled interneurons
    # interneuron: inferred interneurns
    nwbfile = NWBHDF5IO(filename, 'r')
    nwbfile = nwbfile.read()
    cell_types = (
        nwbfile.processing['ophys']
        .data_interfaces['fluorescence_all_cells']
        .roi_response_series['raster dur v26_5_v37_6']
        .control_description[:]
    )
    return cell_types, np.equal(cell_types, 'pyramidal')


def load_raster_cell_type(filename: Path) -> tuple[Afloat, Aint, Aint]:
    """Load the raster for the given cell type.

    Args:
        filename (Path): .nwb filename.

    Returns:
        Afloat: timestamps
        Aint: raster pyramidal cells
        Aint: raster inhibitory neurons

    """
    # load spikes
    ras_t, raster = load_raster(filename)
    _, pyr_cell = load_cell_types(filename)
    return ras_t, raster[pyr_cell], raster[~pyr_cell]


def get_frame_rate(filename: Path) -> float:
    """Get frame rate.

    Args:
        filename (Path): .nwb file.

    Returns:
        float: frame rate in Hz.

    """
    nwbfile = NWBHDF5IO(filename, 'r')
    nwbfile = nwbfile.read()
    return nwbfile.acquisition['motion_corrected_ci_movie'].rate


def load_all_periods(filename: Path) -> Afloat:
    """Load behavior annotations.

    Args:
        filename (Path): .nwb file.

    Returns:
        Afloat: array of limits of movement periods.

    """
    nwbfile = NWBHDF5IO(filename, 'r')
    nwbfile = nwbfile.read()

    labels = list(
        nwbfile.processing['behavior']
        .data_interfaces['BehavioralEpochs']
        .interval_series.keys(),
    )
    sleep_labs = [
        'wake',
        'twitches_blumberg',
        'rem sleep',
        'wake-sleep-transition',
    ]
    mvt_labels = [lab for lab in labels if lab not in sleep_labs]
    return np.concatenate(
        [
            nwbfile.processing['behavior']
            .data_interfaces['BehavioralEpochs']
            .interval_series[lab]
            .timestamps[:]
            .reshape((-1, 2))
            for lab in mvt_labels
        ],
    )


def intervals_conseq(
    timestamps: Afloat,
    intervals: Afloat,
    margin: float = 0.0,
) -> tuple[Abool, Aint]:
    """Compute the intervals where no movement are detected.

    Args:
        timestamps (Afloat): time axis.
        intervals (Afloat): movement time intervals
        margin (float): margin around movements to be ignored.

    Returns:
        Abool: non-movement mask.
        Aint: intervals.

    """
    # offset in secs
    mask_mvt = np.zeros(timestamps.size, dtype=np.bool_)
    for coord in intervals:
        mask_mvt |= np.logical_and(
            timestamps >= coord[0] - margin,
            timestamps <= coord[1] + margin,
        )
    mask_nmvt = ~mask_mvt

    begs = np.nonzero(np.logical_and(~mask_nmvt[:-1], mask_nmvt[1:]))[0] + 1
    ends = np.nonzero(np.logical_and(mask_nmvt[:-1], ~mask_nmvt[1:]))[0] + 1
    if mask_nmvt[0]:
        begs = np.insert(begs, 0, 0)
    if mask_nmvt[-1]:
        ends = np.append(ends, mask_nmvt.size)
    return mask_nmvt, np.vstack((begs, ends)).T


def intervals_mvt(timestamps: Afloat, intervals: Afloat) -> Aint:
    """Compute the intervals where movements are detected.

    Args:
        timestamps (Afloat): time axis.
        intervals (Afloat): movement time intervals

    Returns:
        Aint: intervals.

    """
    # offset in secs
    mask_mvt = np.zeros(timestamps.size, dtype=np.bool_)
    for coord in intervals:
        mask_mvt |= np.logical_and(
            timestamps >= coord[0],
            timestamps <= coord[1],
        )

    begs = np.nonzero(np.logical_and(~mask_mvt[:-1], mask_mvt[1:]))[0] + 1
    ends = np.nonzero(np.logical_and(mask_mvt[:-1], ~mask_mvt[1:]))[0] + 1
    if mask_mvt[0]:
        begs = np.insert(begs, 0, 0)
    if mask_mvt[-1]:
        ends = np.append(ends, mask_mvt.size)
    return np.vstack((begs, ends)).T


def gen_nwb_dict(data_folder: Path) -> dict[str, list[Path]]:
    """Generate of list of .nwb files corresponding to a p_day.

    Args:
        data_folder (Path): folder containing the .nwb files.

    Returns:
        dict: list of filenames for each day.

    """
    nwb_dict: dict[str, list[Path]] = {}
    # loading all session folders
    session_fldr = data_folder.glob('*/')
    for sess in session_fldr:
        nwb_files = sess.glob('*behavior*.nwb')
        for nwb_fi in nwb_files:
            nwb_in = NWBHDF5IO(nwb_fi, 'r').read()
            # Check whether the file contains a raster
            fluo_keys = (
                nwb_in.processing['ophys']
                .data_interfaces['fluorescence_all_cells']
                .roi_response_series.keys()
            )
            if 'raster dur v26_5_v37_6' not in fluo_keys:
                continue
            # reading the p_day
            try:
                nwb_dict[nwb_in.subject.age].append(nwb_fi)
            except KeyError:
                nwb_dict[nwb_in.subject.age] = [nwb_fi]
    return nwb_dict
