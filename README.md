# Somatic inhibition analysis and modeling

![license](https://img.shields.io/badge/license-MIT-green)
![python-version](https://img.shields.io/badge/python-3.9-blue)

Scripts to compute and plot correlation functions and peri-movement time
histograms (PMTHs) about movements

## Setting up Pipenv

You can create a virtual environment through `pipenv` to obtain reproducible
results.
See the [pipenv website](https://pypi.org/project/pipenv/) for installation
instructions.

Once you have a running version of `python 3.9` and `pipenv` install, you can
generate the virtual environment with:

```console
pipenv install
```

## Download the dataset

Once `pipenv` is installed, you can download the dataset with:

```console
pipenv run dandi download -o data DANDI:000219
```

## Generate the correlation functions

Run as:
```console
pipenv run python src/correlations_exp.py
```

The computed correlations and PMTHs are stored in the folder:
`./data`

The figures are stored in the folder:
`./figures`

## Generate the rate model figures

Run as:
```console
pipenv run python src/rate_model.py
```

## Generate the LIF model figures

The leaky integrate and fire (LIF) model is run with:
```console
pipenv run python src/lif_simulation.py
```

The core computations are performed by a C++ extension module that is installed
by pipenv.

### Model

The model consists of $`N_E`$ excitatory and $`N_I`$ inhibitory LIF neurons.

Neuron $`i`$ of population $`A`$ i.e. $`(i, A)`$ receives a connection from
neuron $`(j, B)`$ with a probability given by:
```math
P(C_{AB}^{ij} = 1) = \frac{K}{N_B}
```

The membrane voltage evolves as follows,
```math
\frac{d}{dt} V_{A}^{i} =
    \frac{-1}{\tau_{mem}} V_A^i + I_{rec}^i + I^i_{FF}
    + \sigma \eta(t)
```

Recurrent input $`I_{rec}^i`$ depends on all presynatic spikes:
```math
I_{rec}^i =
    \sum_{B = (E, I)} J_{AB} \sum_j \, C_{AB}^{ij} \sum_k
    \exp(- \frac{t - t^j_k}{\tau_B})
```

$`\tau`$ is the decay constant of the exponential synapses.

Recurrent weights are, $` J_{EE}`$,  $` J_{EI}`$, $` J_{IE}`$,  $` J_{II}`$

The network receives constant feedforward input whose rate is set to
$` v_{ext} `$.
The feed-forward weights are $`J_{E0}`$, $`J_{I0}`$.

$`\eta(t)`$ is the temporal white noise, i.e. $`\langle \eta(t) \rangle_t = 0`$
and $`\langle \eta(t) \eta(t^{\prime})\rangle_t = \delta(t - t^{\prime})`$.
$`\sigma`$ is the power of noise, this is set with parameter `noise_power`

## Parameters

The parameters for the different models are located in the `src/config` folder.
