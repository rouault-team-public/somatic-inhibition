"""Legacy script setup.py to support c++ module extensions."""
import numpy
import setuptools  # type: ignore

lifsimu = setuptools.Extension(
    'lifsimu',
    sources=[
        'src/liblif/simlifmodule.cpp',
        'src/liblif/utils.cpp',
    ],
    include_dirs=[numpy.get_include()],
    extra_compile_args=['-march=native', '-O3'],
)

setuptools.setup(
    ext_modules=[lifsimu],
)
